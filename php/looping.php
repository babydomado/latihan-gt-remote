<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>berlatih looping php</h1>
    <?php
    echo "<h3>contoh 1</h3>";
    echo "<h5>looping 1</h5>";
    for($i=2; $i<=20; $i+=1){
        echo $i . "- i love php <br>";

    }
    echo "<h5>looping 2</h5>";
    for ($a=20; $a>=2; $a-=1){
        echo $a . "- i love php <br>";
    }

    echo "<h3>contoh 2</h3>";
    $angka = [45,67,86,90,54];
    echo "array angka : ";
    print_r($angka);
    echo "<br>";
    echo "hasil sisa dari array angka";
    foreach ($angka as $value) {
        $rest[] = $value %= 6;
    }
    print_r($rest);
    echo "<br>";

    echo "<h3>contoh 3</h3>";
    $biodata = [
        ["abdul", 27, "jakarta"],
        ["ahmad", 28, "makasar"],
        ["yoga", 29, "bekasi"],
    ];
    foreach ($biodata as $key => $value) {
       $item = array(
        'nama' => $value[0],
        'umur' => $value[1],
        'alamat' => $value[2],
       );
       print_r($item);
       echo "<br>";
    }

    echo "<h3>contoh 4</h3>";
    for ($j=1; $j<=5; $j++){
        for ($b=1; $b<=$j; $b++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>