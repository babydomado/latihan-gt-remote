<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>hello world</h2>
    <?php
    echo "<h3>contoh 1</h3>";
    $trainer = ["mike", "arya", "mike", "farrel", "abdul"];
    print_r($trainer);

    echo "<h3>contoh 2</h3>";
    echo "total trainer ". count($trainer);
    echo "<ol>";
    echo "<li>" . $trainer[0] ."</li>";
    echo "<li>" . $trainer[1] ."</li>";
    echo "<li>" . $trainer[2] ."</li>";
    echo "<li>" . $trainer[3] ."</li>";
    echo "<li>" . $trainer[4] ."</li>";
    echo "</ol>";

    echo "<h3>contoh 3</h3>";
    $biodata = [
        ["rezky", "26", "laravel"],
        ["rio", "27", "python"],
        ["abdul", "21", "digital"]
    ];
    echo "<pre>";
    print_r($biodata);
    echo "</pre>";
    ?>
</body>
</html>