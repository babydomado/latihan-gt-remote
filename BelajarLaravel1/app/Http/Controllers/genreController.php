<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class genreController extends Controller
{
    public function create()
    {
        return view('genre.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
    }
}
