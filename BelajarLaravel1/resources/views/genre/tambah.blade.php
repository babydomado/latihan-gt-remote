@extends('layout.master')
@section('judul')
Halaman tambah genre
@endsection
@section('content')

<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>nama genre</label>
      <input name="nama" type="text" class="form-control">
    </div>
    <div class="form-group">
      <label>description genre</label>
      <textarea name="descripsi" class="form-control" cols="30" rows="10"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

